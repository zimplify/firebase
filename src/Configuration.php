<?php
    namespace Zimplify\Firebase;
    use Zimplify\Firebase\Providers\RemoteConfigProvider;
    use Kreait\Firebase\RemoteConfig\{Condition, ConditionalValue, Parameter, TagColor, Template};

    /**
     * the RemoteConfig instance helps us in managing a new template and send it to Firebase
     * @package Zimplify\Firebase (code 24)
     * @type Unpersisted (code 13)
     * @file Configuration (code 01)
     */
    class Configuration {
        
        private $version;
        private $provider;
        public $template;
        private $conditions = [];
        private $parameters = [];

        /**
         * creating a new instance for us to issue templates
         * @param RemoteConfigProvider $controller the provider that will deal with the sync.
         * @return void
         */
        function __construct(RemoteConfigProvider $controller) {
            $this->provider = $controller;
            $this->template = Template::new();
        }

        /**
         * adding new parameter into the configuration
         * @param string $field the name of the field pair
         * @param mixed $default the value to set
         * @param string $condition (optional) the index of condition (must be defined before adding)
         * @param string $description (optional) the description to use
         * @return bool
         */
        public function add(string $field, $default, array $conditions = [], string $description = null) : self {
            $result = Parameter::named($field)->withDefaultValue($default);
            if (!is_null($description)) $result = $result->withDescription($description);

            // this is the conditional part
            foreach ($conditions as $condition => $value) {                
                if (array_key_exists($condition, $this->conditions) && !is_null($value)) {                                
                    $actual = $this->conditions[$condition];
                    $matched = ConditionalValue::basedOn($actual)->withValue($value);
                    $result = $result->withConditionalValue($matched);
                }
            }

            // add to our parameters
            $this->parameters[$field] = $result;
            
            // now return
            return $this;
        }

        /**
         * adding a new condition to the configuration
         * @param string $name the name (or label) for the condition
         * @param string $expression the condition we will evaluate
         * @param string $tag (optional) the color tag to display on console @firebase
         * @return Condition
         */
        public function assume(string $name, string $expression, string $tag = null) : self {
            $result = Condition::named($name);
            $result = $result->withExpression($expression);
            if (!is_null($tag)) {
                $color = constant("Kreait\Firebase\RemoteConfig\TagColor::$tag");
                $result = $result->withTagColor($color);
            }
            $this->conditions[$name] = $result;
            return $this;
        }

        /**
         * this is our publishing side
         * @return true
         */
        public function save() : bool {
            foreach ($this->conditions as $name => $c) $this->template = $this->template->withCondition($c);
            foreach ($this->parameters as $name => $p) $this->template = $this->template->withParameter($p);
            return $this->provider->sync($this);
        }
    }
<?php
    namespace Zimplify\Firebase\Traits;
    use Zimplify\Core\Application;
    use Zimplify\Firebase\Interfaces\IFirebaseServicesInterface;
    use Kreait\Firebase\Factory;
    use \RuntimeException;

    /**
     * this trait handles the initialization of Firebase SDK
     * @package Zimplify\Firebase (code 24)
     * @type Trait (code 07)
     * @file TFirebaseHandler (code 01)
     */
    trait TFirebaseHandler {
        
        /**
         * our main method to initialize te Firebase factory
         * @return Factory
         */
        public function prepare() : Factory {
            $token = Application::env(IFirebaseServicesInterface::CFG_FIREBASE_TOKEN);
            $project = Application::env(IFirebaseServicesInterface::CFG_FIREBASE_TOKEN);

            if (!is_null($token)) {
                $result = (new Factory);

                // adding the debug flag here if we are in debug
                if (array_key_exists("debug", $this->driver())) {                    
                    if ($this->driver()["debug"] === true) {
                        $this->debug("Provider activating HTTP tracing.", __FUNCTION__);
                        $result = $result->withHttpClientConfig(['debug' => true]);
                    }
                }

                // setting the service account
                $result = $result->withServiceAccount($token);

                // activating the datastore dir if we are using RTD
                if (!is_null($project)) 
                    $result = $result->withDatabaseUri($project);

                return $result;
            } else  
                throw new RuntimeException("Unable to discover service account.", self::ERR_BAD_ACCOUNT);
        }
    }
<?php
    namespace Zimplify\Firebase\Interfaces;
    use Kreait\Firebase\Factory;

    /**
     * this interface defines the basic connections for providers that uses Firebase Service
     * @package Zimplify\Firebase (code 24)
     * @type Interface (code 06)
     * @file IFirebaseServiceInterface (code 01)
     */
    interface IFirebaseServicesInterface {

        const CFG_FIREBASE_TOKEN = "vendor.firebase.token";
        const CFG_FIREBASE_STORE = "vendor.firebase.project";
        const ERR_BAD_ACCOUNT = 500240601001;

        /**
         * our main method to initialize te Firebase factory
         * @return Factory
         */
        function prepare() : Factory;
        
    }
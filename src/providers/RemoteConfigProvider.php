<?php
    namespace Zimplify\Firebase\Providers;
    use Zimplify\Core\{Application, Provider};
    use Zimplify\Firebase\Configuration;
    use Zimplify\Firebase\Interfaces\IFirebaseServicesInterface;
    use Zimplify\Firebase\Traits\TFirebaseHandler;
    use Kreait\Firebase\RemoteConfig;
    use \RuntimeException;
    
    /**
     * this Provider allow us to create, publish and alternate remote config
     * @package Zimplify\Firebase (code 24)
     * @type Provider (code 03)
     * @file RemoteConfigProvider
     */
    class RemoteConfigProvider extends Provider implements IFirebaseServicesInterface {
        use TFirebaseHandler;

        const DEF_CLS_NAME = "Zimplify\\Firebase\\Providers\\RemoteConfigProvider";
        const DEF_SHT_NAME = "firebase::remote-config";

        private $client;

        /**
         * create a new configuration for drafting
         * @return Configuration
         */
        public function draft() : Configuration {
            return new Configuration($this);
        }
    
        /**
         * initializing the provider.
         * @return void
         */
        protected function initialize() {
            parent::initialize();
            $this->client = $this->prepare()->createRemoteConfig();
        }
    
        /**
         * check if all required arguments are supplied into the provider for initialization.
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }

        /**
         * this is our main function to sync data with Firebase
         * @param Configuration provide a configurartion set
         * @return bool
         */
        public function sync(Configuration $data) : bool {
            try {
                $this->client->publish($data->template);
                return true;
            } catch (Exception $ex) {
                $this->log($ex);
                return false;
            }
        }
    }
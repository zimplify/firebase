<?php
    namespace Zimplify\Firebase\Providers;
    use Zimplify\Core\{Application, Provider, Query};
    use Zimplify\Firebase\Interfaces\IFirebaseServicesInterface;
    use Zimplify\Firebase\Traits\TFirebaseHandler;
    use Zimplify\Messaging\{Message};
    use Zimplify\Messaging\Interfaces\ISendableInterface;
    use Kreait\Firebase\Messaging\{CloudMessage, Notification};
    use \RuntimeException;
    
    /**
     * the connects us to FCM on Firebase
     * @package Zimplify\Firebase (code 24)
     * @type Provider (code 03)
     * @file CloudMessagingProvider (code 01)
     */
    class CloudMessagingProvider extends Provider implements IFirebaseServicesInterface, ISendableInterface {
        use TFirebaseHandler;

        const ARGS_TOKEN = "token";
        const ARGS_NOTIFICATION = "notification";
        const ARGS_EXTRA_DATA = "data";
        const DEF_CLS_NAME = "Zimplify\\Firebase\\Providers\\CloudMessagingProvider";
        const DEF_SHT_NAME = "firebase::cloud-messaging";
        const DRV_ARGS_LOOPBACK = "loopback";
        const ERR_BAD_MESSAGE = 500240301001;
        const ERR_NO_RECEIVERS = 500240301002;

        private $client;

        /**
         * encoding the Zimplify into the format needed by Firebase
         * @param Message $message the message we want to send
         * @return Notification
         */
        // protected function encode(Message $message) : Notification {
        protected function encode(Message $message) : array {
            $result = [];
            $result["title"] = $message->title;
            $result["body"] = $message->body;
            if (!is_null($message->image)) 
                $result["image"] = $message->image;

            // now return
            return $result;
        }

        /**
         * Building our message
         * @param array $message the physical message data
         * @return CloudMessage
         */
        protected function generate(array $message) : CloudMessage {
            if (array_key_exists(self::ARGS_TOKEN, $message) && 
                array_key_exists(self::ARGS_NOTIFICATION, $message) &&
                array_key_exists(self::ARGS_EXTRA_DATA, $message)) 
                return CloudMessage::fromArray($message);
            else 
                throw new RuntimeException("Unable to encode CM.", self::ERR_BAD_MESSAGE);
        }

        /**
         * loading up the device id of the receiver(s)
         * @param Message $message the message object to send
         * @return string
         */
        protected function identify(Message $message) : string {
            $result = "";
            foreach ($message->receivers as $receipients) 
                if (count($target = Application::search([Query::SRF_ID => $receipients])) > 0) 
                    if (!is_null($device = $target[0]->device)) 
                        $result = $device;
                    elseif (array_key_exists(self::DRV_ARGS_LOOPBACK, $this->driver()) && $this->driver()[self::DRV_ARGS_LOOPBACK] === true) 
                        $result = "test.client";
            
            if (strlen($result) == 0) 
                throw new RuntimeException("No receipients in the list.", self::ERR_NO_RECEIVERS);
            
            return $result;
        }

        /**
         * initializing the provider.
         * @return void
         */
        protected function initialize() {
            parent::initialize();
            $this->client = $this->prepare()->createMessaging();
        }
    
        /**
         * check if all required arguments are supplied into the provider for initialization.
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }

        /**
         * sending out the message
         * @param Message $message the message to send out
         * @return bool
         */
        public function send(Message $message) : bool {
            $notification = $this->encode($message);
            $extra = $message->extra;
            $device = $this->identify($message);

            // now we need to generate our message
            $message = $this->generate([self::ARGS_TOKEN => $device, self::ARGS_NOTIFICATION => $notification, self::ARGS_EXTRA_DATA => $extra]);

            // now we send the request
            $result = $this->client->send($message);

            // now return the data
            return true;
        }
    }
